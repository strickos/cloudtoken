#!/usr/bin/env bash

set -xeuo pipefail

# Change this when you need to increase the version significantly. Otherwise the build number is used for
# general bugfix version bumps.
PACKAGE_VERSION="${BASE_VERSION}.${BITBUCKET_BUILD_NUMBER}"

BUILD_ENV="${1}"

if [ "${BUILD_ENV}" == "master" ]; then
    PYPI_ENV="pypi"
    export VERSION="${PACKAGE_VERSION}"
elif [ "${BUILD_ENV}" == "development" ]; then
    PYPI_ENV="pypi"
    # PYPI_ENV="pypitest"  # Uncomment if you want to push dev packages to testpypi instead.
    export VERSION="${PACKAGE_VERSION}.dev1"
else
    echo "Unknown build environment. Exiting."
    exit 1
fi

# Configure PyPi details.
cat << EOF > ~/.pypirc
[distutils]
index-servers =
    pypi
    pypitest

[pypi]
username:${PYPI_USERNAME}
password:${PYPI_PASSWORD}

[pypitest]
repository: https://test.pypi.org/legacy/
username:${PYPITEST_USERNAME}
password:${PYPITEST_PASSWORD}
EOF

echo "Beginning build for version: ${VERSION}."

# Make sure we are running the latest versions.
pip3 install -U pip setuptools wheel twine

echo "Building main cloudtoken app package and pushing to ${PYPI_ENV}."
pushd cloudtoken
/var/lang/bin/python3 ./setup.py bdist_wheel sdist
twine upload -r "${PYPI_ENV}" dist/*
popd

# Build plugin packages.
for PLUGIN in $(find ./plugins/* -maxdepth 0 -type d | cut -d '/' -f 3); do
    echo "Building ${PLUGIN} plugin package and pushing to ${PYPI_ENV}."
    pushd "plugins/${PLUGIN}"
    /var/lang/bin/python3 ./setup.py bdist_wheel sdist
    twine upload -r "${PYPI_ENV}" dist/*
    popd
done
